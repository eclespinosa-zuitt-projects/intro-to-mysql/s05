-- 1.
SELECT customerName
  FROM customers
 WHERE country = "Philippines";

-- 2.
SELECT contactLastName, contactFirstName
  FROM customers
 WHERE customerName = "La Rochelle Gifts";

-- 3.
SELECT productName, MSRP
  FROM products
 WHERE productName = "The Titanic";

-- 4.
SELECT firstName, lastName
  FROM employees
 WHERE email = "jfirrelli@classicmodelcars.com";

-- 5.
SELECT customerName
  FROM customers
 WHERE state IS NULL;

-- 6.
SELECT firstName, lastName
  FROM employees
 WHERE lastName = "Patterson"
   AND firstName = "Steve";

-- 7.
SELECT customerName, country, creditLimit
  FROM customers
 WHERE country != "USA"
   AND creditLimit > 3000;

-- 8.
SELECT customerName
  FROM customers
 WHERE customerName NOT LIKE "%a%";

-- 9.
SELECT customerNumber
  FROM orders
 WHERE comments LIKE '%DHL%';

-- 10.
SELECT productLine
  FROM productlines
 WHERE textDescription LIKE "%state of the art%";

-- 11.
SELECT DISTINCT country
  FROM customers;

-- 12.
SELECT DISTINCT status
  FROM orders;

-- 13.
SELECT customerName
  FROM customers
 WHERE country IN ("USA", "France", "Canada");

-- 14.
SELECT a.firstName, a.lastName, b.city
  FROM employees AS a
  JOIN offices AS b
    ON a.officeCode = b.officeCode
 WHERE b.city = "Tokyo";

-- 15.
SELECT a.customerName
  FROM customers AS a
  JOIN employees AS b
    ON a.salesRepEmployeeNumber = b.employeeNumber
 WHERE b.firstName = "Leslie"
   AND b.lastName = "Thompson";

-- 16.
SELECT b.productName, a.customerName
  FROM customers AS a
  JOIN orders AS c
    ON a.customerNumber = c.customerNumber
  JOIN orderdetails AS d
    ON c.orderNumber = d.orderNumber
  JOIN products AS b
    ON b.productCode = d.productCode
 WHERE a.customerName = "Baane Mini Imports";

-- 17.
SELECT a.firstName, a.lastName, b.customerName, b.country
 FROM employees AS a
 JOIN customers AS b
   ON a.employeeNumber = b.salesRepEmployeeNumber
 JOIN offices AS c
   ON a.officeCode = c.officeCode
WHERE b.country = c.country;

-- 18.
SELECT lastName, firstName
  FROM employees
 WHERE reportsTo IN ( SELECT employeeNumber
                        FROM employees
                       WHERE firstName = "Anthony"
                         AND lastName = "Bow" );

-- 19.
SELECT productName, MSRP
  FROM products
  WHERE MSRP IN (SELECT MAX(MSRP) FROM products);

-- 20.
SELECT COUNT(*)
  FROM customers
 WHERE country = "UK";

-- 21.
SELECT productLine, COUNT(productName) AS number_of_products
  FROM products
 GROUP BY productLine;

-- 22.
SELECT b.firstName, b.lastName, COUNT(a.customerName) AS number_of_customers_served
  FROM customers AS a
  LEFT OUTER JOIN employees AS b
    ON a.salesRepEmployeeNumber = b.employeeNumber
 GROUP BY b.employeeNumber;

-- 23.
SELECT productName, quantityInStock
FROM products
WHERE productLine = "Planes"
AND quantityInStock < 1000;